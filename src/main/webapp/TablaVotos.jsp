<%@page import="java.util.List" %>
<%@page import="PlayerObject.Player" %>
<!DOCTYPE html>
<html lang="es"></html>
<html>
    <head>
        <title>Listado de votos</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h2>Listado de votos </h2>
        <main>
            <table id="votesTable">
                <thead>
                    <tr>
                        <th>Jugador</th>
                        <th>Votos</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        List<Player> players = (List<Player>) session.getAttribute("listPlayers");
                        for (Player p : players){
                    %>
                            <tr>
                                <td><%= p.getName() %></td>
                                <td><%= p.getVotes() %></td>
                            </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
        </main>
        <br>
        <br> <a style="color:white;" href="index.html"> Ir al comienzo</a>
    </body>
</html>