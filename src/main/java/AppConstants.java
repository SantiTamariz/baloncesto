public class AppConstants {

    private AppConstants() {
        throw new IllegalStateException("Utility class");
      }

    public static final String ERROR="El error es ";
    public static final String NO_TABLE="No lee de la tabla";
    public static final String NO_CONNECTION="No se ha podido conectar";
    public static final String NO_INSERT="No inserta en la tabla";
    public static final String NO_VALUE_TO_ZERO="No cambia valores a 0";
}
