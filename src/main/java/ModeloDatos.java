import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import PlayerObject.Player;

public class ModeloDatos {

    private Connection con;
    private Statement set;
    private ResultSet rs;

    static Logger logger = Logger.getLogger(ModeloDatos.class.getName());

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            logger.log(Level.CONFIG, url);
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            logger.log(Level.WARNING, AppConstants.NO_CONNECTION);
            logger.log(Level.WARNING, AppConstants.ERROR + e.getMessage());
        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            logger.log(Level.WARNING,AppConstants.NO_TABLE);
            logger.log(Level.WARNING, AppConstants.ERROR + e.getMessage());
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            logger.log(Level.WARNING,"No modifica la tabla");
            logger.log(Level.WARNING, AppConstants.ERROR + e.getMessage());
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            logger.log(Level.WARNING,AppConstants.NO_INSERT);
            logger.log(Level.WARNING, AppConstants.ERROR + e.getMessage());
        }
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void cleanVotes(){
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores set votos = 0");
            rs.close();
            set.close();
        } catch (Exception e) {
            logger.log(Level.WARNING,AppConstants.NO_VALUE_TO_ZERO);
            logger.log(Level.WARNING, AppConstants.ERROR + e.getMessage());
        }
    }

    public int getVotos(String nombre){
        int votos = 0;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT votos FROM Jugadores WHERE nombre " + " LIKE '%" + nombre + "%'");
            votos = rs.getInt("votos");
            rs.close();
            set.close();
        } catch (Exception e) {
            logger.log(Level.WARNING,AppConstants.NO_TABLE);
            logger.log(Level.WARNING, AppConstants.ERROR + e.getMessage());
        }
        return (votos);
    }

    public Player getJugador(String nombreJugador)
    {
        Player player = new Player();
        try {
            abrirConexion();
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores WHERE nombre='" + nombreJugador + "';");
            while (rs.next()) {
                player.setName(rs.getString("nombre"));
                player.setVotes(rs.getInt("votos"));
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            System.out.println("No se ha conseguido acceder a la tabla de Jugadores. El error es: " + e.getMessage());
        }

        return player;
    }

    public List<Player> getPLayers(){
        List<Player> players = new LinkedList<>();
        String name;
        int votes;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                name = rs.getString("nombre");
                votes = rs.getInt("votos");
                players.add(new Player(name, votes));
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            logger.log(Level.WARNING,AppConstants.NO_TABLE);
            logger.log(Level.WARNING, AppConstants.ERROR + e.getMessage());
        }
        return players;
    }

}
