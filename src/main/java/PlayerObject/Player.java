package PlayerObject;
public class Player {
    private String name;
    private int votes;

    public Player(String name, int votes) {
        this.name = name;
        this.votes = votes;
    }

    public Player() {
    }

    public String getName() {
        return name;
    }

    public int getVotes() {
        return votes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }


}
