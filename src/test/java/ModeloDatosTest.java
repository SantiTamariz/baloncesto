import org.junit.jupiter.api.Test;

import PlayerObject.Player;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;

public class ModeloDatosTest {

    private static final ModeloDatos instance = new ModeloDatos();
    
    @BeforeAll
    public static void initBD(){
        instance.abrirConexion();
    }
    
    @Test
    public void testExisteJugador() {

        System.out.println("Prueba de existeJugador");
        String nombre = "";
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
    }

    @Test
    public void testActualizarJugador2() {
        System.out.println("Prueba de actualizarJugador");
        String nombre = "Carroll";
        ModeloDatos instance = new ModeloDatos();
        Player playerInitial = instance.getJugador(nombre);
        instance.actualizarJugador(nombre);
        Player playerFinal = instance.getJugador(nombre);
        assertEquals(playerInitial.getVotes(), playerFinal.getVotes());
    }

}