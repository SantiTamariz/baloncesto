import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

public class PruebasPhantomjsIT {
      private static WebDriver driver = null;

      @Test
      public void tituloIndexTest() {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setJavascriptEnabled(true);
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                        new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
            driver = new PhantomJSDriver(caps);
            driver.navigate().to("http://localhost:8080/Baloncesto/");
            assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(),
                        "El titulo no es correcto");
            System.out.println(driver.getTitle());
            driver.close();
            driver.quit();
      }

      @Test
      public void resetVotesTest() {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setJavascriptEnabled(true);
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                        new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
            driver = new PhantomJSDriver(caps);
            driver.navigate().to("http://localhost:8080/Baloncesto/");
            // busca el botón "Poner votos a cero" y pulsa en él
            WebElement resetButton = driver.findElement(By.name("B3"));
            resetButton.click();
            // busca el botón "Ver votos" y pulsa en él
            WebElement viewVotesButton = driver.findElement(By.name("B4"));
            viewVotesButton.click();
            // Busca la tabla que contiene los votos de los jugadores
            WebDriverWait wait = new WebDriverWait(driver, 10);
            WebElement votesTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id='votesTable']")));
            List<WebElement> rows = votesTable.findElement(By.tagName("tbody")).findElements(By.tagName("tr"));

            // comprueba que todos los votos están a 0
            for (WebElement row : rows) {
                  List<WebElement> cols = row.findElements(By.tagName("td"));
                  assertEquals("0", cols.get(1).getText(), "The votes should be 0");
            }
            driver.close();
            driver.quit();
      }

      @Test
      public void testAddNewPlayerAndVote() {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setJavascriptEnabled(true);
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                        new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
            driver = new PhantomJSDriver(caps);
            driver.navigate().to("http://localhost:8080/Baloncesto/");
            // Introduce el nombre del nuevo jugador
            driver.findElement(By.name("txtNombre")).sendKeys("NewPlayer");

            // Selecciona la opción "Otros" 
            driver.findElement(By.name("R1")).findElement(By.xpath("//input[@value='Otros']")).click();

            // Introduce el nombre
            driver.findElement(By.name("txtOtros")).sendKeys("NewPlayer");

            // Clica en el boton de votar
            driver.findElement(By.name("B1")).click();

            // Vuelve a la página principal
            driver.navigate().to("http://localhost:8080/Baloncesto/");

            // Presiona en el botón de ver votos
            driver.findElement(By.name("B4")).click();

            // Recupera los votos para el usuario añadido
            String voteCount = driver.findElement(By.xpath("//td[text()='NewPlayer']/following-sibling::td")).getText();

            // Comprueba que el número de votos es 1
            assertEquals("1", voteCount, "New player's vote count is not 1");

            driver.close();
            driver.quit();
      }
}